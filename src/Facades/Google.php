<?php

namespace ABDev\GoogleAPIClient\Facades;

use ABDev\GoogleAPIClient\Client;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin Client
 */
class Google extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Client::class;
    }
}
